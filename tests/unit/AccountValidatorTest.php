<?php
namespace Tests\unit;

use Codeception\Test\Unit;
use TSP\Entities\Account\AccountValidator;
use TSP\Entities\Account\Exceptions\BirthDateBadFormatException;
use TSP\Entities\Account\Exceptions\EmailBadFormatException;
use TSP\Entities\Account\Exceptions\LoginBadFormatException;
use TSP\Entities\Account\Exceptions\PasswordBadFormatException;
use TSP\Entities\Account\Exceptions\UserNameBadFormatException;

class AccountValidatorTest extends Unit
{
    /** @var AccountValidator */
    protected $validator;
    
    public function _before()
    {
        $this->validator = new AccountValidator();
    }
    
    public function testValidateLoginFailed_1()
    {
        $this->expectException(LoginBadFormatException::class);
        $login = 'alai';
        $this->validator->validateLogin($login);
    }

    public function testValidateLoginFailed_2()
    {
        $this->expectException(LoginBadFormatException::class);
        $login = 'alain_alain_alain_alain_alain_alain_alain_alain';
        $this->validator->validateLogin($login);
    }

    public function testValidateLoginFailed_3()
    {
        $this->expectException(LoginBadFormatException::class);
        $login = 'alain alain';
        $this->validator->validateLogin($login);
    }

    public function testValidateLoginFailed_4()
    {
        $this->expectException(LoginBadFormatException::class);
        $login = 'alain@alain';
        $this->validator->validateLogin($login);
    }

    public function testValidateLoginOK_1()
    {
        $loginTooLong = 'alain.dupont';
        $this->assertTrue($this->validator->validateLogin($loginTooLong));
    }

    public function testValidatePasswordFailed_1()
    {
        $this->expectException(PasswordBadFormatException::class);
        $pwdTooShort = 'alain';
        $this->validator->validatePassword($pwdTooShort);
    }

    public function testValidatePasswordFailed_2()
    {
        $this->expectException(PasswordBadFormatException::class);
        $pwdTooLong = 'alain_alain_alain_alain_alain_alain_alain_alain';
        $this->validator->validatePassword($pwdTooLong);
    }
    
    public function testValidatePasswordFailed_3()
    {
        $this->expectException(PasswordBadFormatException::class);
        $pwdNoUpperCase = 'alain alain';
        $this->validator->validatePassword($pwdNoUpperCase);
    }

    public function testValidatePasswordFailed_4()
    {
        $this->expectException(PasswordBadFormatException::class);
        $pwdNoLowerCase = 'PASSWORD';
        $this->validator->validatePassword($pwdNoLowerCase);
    }

    public function testValidatePasswordFailed_5()
    {
        $this->expectException(PasswordBadFormatException::class);
        $pwdNoNumber = 'Password';
        $this->validator->validatePassword($pwdNoNumber);
    }

    public function testValidatePasswordFailed_6()
    {
        $this->expectException(PasswordBadFormatException::class);
        $pwdNoSpecialChars = 'Password1';
        $this->validator->validatePassword($pwdNoSpecialChars);
    }
    
    public function testValidatePasswordOK_1()
    {
        $passwordOk = 'Yla$tL1';
        $this->assertTrue($this->validator->validatePassword($passwordOk));
    }

    public function testValidateEmailFailed_1()
    {
        $this->expectException(EmailBadFormatException::class);
        $email = 'alain@gmail';
        $this->validator->validateEmail($email);
    }
    public function testValidateEmailFailed_2()
    {
        $this->expectException(EmailBadFormatException::class);
        $email = 'alain@gmail.';
        $this->validator->validateEmail($email);
    }
    public function testValidateEmailFailed_3()
    {
        $this->expectException(EmailBadFormatException::class);
        $email = '@gmail.fr';
        $this->validator->validateEmail($email);
    }
    public function testValidateEmailFailed_4()
    {
        $this->expectException(EmailBadFormatException::class);
        $email = 'alain.gmail.fr';
        $this->validator->validateEmail($email);
    }
    public function testValidateEmailOk_1()
    {
        $email = 'alain.dupont@gmail.fr';
        $this->assertTrue($this->validator->validateEmail($email));
    }
    public function testValidateEmailOk_2()
    {
        $email = 'alain.dupont@gmail.co.uk';
        $this->assertTrue($this->validator->validateEmail($email));
    }
    public function testValidateEmailOk_3()
    {
        $email = 'alain_dupont@gmail.co.uk';
        $this->assertTrue($this->validator->validateEmail($email));
    }


    public function testValidatorUsernameFailed_1()
    {
        $this->expectException(UserNameBadFormatException::class);
        $userName = 'ala';
        $this->validator->validateUserName($userName);
    }

    public function testValidatorUsernameFailed_2()
    {
        $this->expectException(UserNameBadFormatException::class);
        $userName = 'alain_alain_alain_alain_alain_alain_alain_alain';
        $this->validator->validateUserName($userName);
    }

    public function testValidatorUsernameFailed_3()
    {
        $this->expectException(UserNameBadFormatException::class);
        $userName = '!alain>dupont';
        $this->validator->validateUserName($userName);
    }

    public function testValidatorUsernameOk_1()
    {
        $userName = 'alain alain';
        $this->assertTrue($this->validator->validateUserName($userName));
    }
    public function testValidatorUsernameOk_2()
    {
        $userName = 'alain@alain';
        $this->assertTrue($this->validator->validateUserName($userName));
    }
    public function testValidatorUsernameOk_3()
    {
        $userName = 'alain.dupont';
        $this->assertTrue($this->validator->validateUserName($userName));
    }

    public function testValidatorBirthDate()
    {
        $date = new \DateTime('tomorrow');
        $this->assertFalse($this->validator->validateBirthDate($date));

        $date = '2015-10-12';
        $this->assertTrue($this->validator->validateBirthDate($date));

        $date = new \DateTime('now');
        $date->modify("+1 days");
        $this->assertFalse($this->validator->validateBirthDate($date));
    }

    public function testValidatorBirthDateFailed_1() {
        $this->expectException(BirthDateBadFormatException::class);
        $date = '2015-10-12 10:00';
        $this->validator->validateBirthDate($date);
    }

}