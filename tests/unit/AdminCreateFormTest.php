<?php
namespace Tests\unit;

use Codeception\Test\Unit;
use TSP\Actors\Admin\UseCases\CreateForm\Boundaries\RepositoryInterface;
use TSP\Actors\Admin\UseCases\CreateForm\RequestData;
use TSP\Actors\Admin\UseCases\CreateForm\ResponseData;
use TSP\Actors\Admin\UseCases\CreateForm\CreateForm;
use TSP\Actors\BoundaryDataFactory;
use TSP\Entities\Form\FormWorker;
use TSP\TestApi\DataModelHelper;

class AdminCreateFormTest extends Unit
{
    /**
     * @var CreateForm
     */
    protected $useCase;

    /**
     * @var RequestData
     */
    protected $requestData;

    /**
     * @var ResponseData
     */
    protected $responseData;

    /**
     * @var  RepositoryInterface
     */
    protected $repository;

    /**
     * @var array
     */
    protected $requestDataStruct;

    /**
     * @var DataModelHelper
     */
    protected $dataStructHelper;

    /**
     * @var array
     */
    protected $responseDataStruct;

    /** @var FormWorker */
    protected $formWorker;

    public function setUp()
    {
        # set up objects
        $this->repository = $this->getMockForAbstractClass(RepositoryInterface::class);

        # setup data structure
        $this->dataStructHelper = new DataModelHelper(new BoundaryDataFactory(), 'CreateForm');

        $this->formWorker = new FormWorker();
    }


    public function testCreateFormSimpleRegular()
    {
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();

        // manual setup data
        $request->formSteps    = ['step_1'];
        $request->formSections = ['step_1'    => ['section_1'] ];
        $request->formFields   = ['section_1' =>
                                        [
                                            'field_1' => ['TextField' => 'Core'],
                                            'field_2' => ['TextField' => 'Core']
                                        ]
                                  ];

            var_dump($request);

        # prepare response data
        $formReturned = $this->formWorker->buildForm($request->formSteps, $request->formSections, $request->formFields);
        $formReturned->setName($request->formName);
        var_dump($formReturned);

        # prepare internal components behaviour
        $this->repository->expects($this->exactly(1))
            ->method('persist')
            ->will($this->returnValue($formReturned));

        # invoke use case
        $this->useCase      = new CreateForm($this->repository);
        /** @var ResponseData $response */
        $response = $this->useCase->doCreateForm($request);
        var_dump($response->formJson);

        # assert response has same mandatory properties then request
        $assertable = $request->getMandatoryFields();
        foreach ($assertable as $field) {
            $this->assertEquals($response->$field, $request->$field);
        }

    }

}