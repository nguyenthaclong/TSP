<?php
namespace Tests\unit;
use Codeception\Test\Unit;
use TSP\Entities\Form\Form;
use TSP\Entities\Form\FormSection;
use TSP\Entities\Form\FormSectionCollection;
use TSP\Entities\Form\FormStep;
use TSP\Entities\Form\FormStepCollection;
use TSP\Entities\Form\FormWorker;

class FormWorkerTest extends Unit
{
    /** @var FormWorker */
    private $formWorker;

    public function _before()
    {
        $this->formWorker = new FormWorker();
    }


    private function getInputDataSimple_ValidFormat() {
        return ['steps'    => ['step_1'],
                'sections' => ['step_1' => ['section_1'] ],
                'fields'   => ['section_1' =>
                                  [
                                   'field_1' => ['TextField'        => 'Core'],
                                   'field_2' => ['TextField'        => 'Core']
                                  ]
                              ]
               ];
    }

    public function testCreationFormSimple_1()
    {
        $data = $this->getInputDataSimple_ValidFormat();

        # get form
        $form = $this->buildForm($data);

        # get num step
        $this->assertEquals(1, $this->getNumSteps($form));

        # get num section of step 1
        $steps = $form->getSteps();
        $steps->getIterator()->rewind();
        $oneStep = $steps->getIterator()->current();
        $this->assertEquals(1, $this->getNumSections($oneStep));

        # get num field of step 1 / section 1
        $sections = $oneStep->getSections();
        $sections->getIterator()->rewind();
        $oneSection = $sections->getIterator()->current();
        $this->assertEquals(2, $this->getNumFields($oneSection));
    }


    private function getInputDataSimple_WrongStepID() {
        return ['steps'    => ['step_1'],
            'sections' => ['step_0' => ['section_1'] ],
            'fields'   => ['section_1' =>
                [
                    'field1' => ['TextField'        => 'Core']
                ]
            ]
        ];
    }

    public function testCreationFormSimple_WithWrongStepName()
    {
        $data = $this->getInputDataSimple_WrongStepID();

        # get form
        $form = $this->buildForm($data);

        # get num step
        $this->assertEquals(1, $this->getNumSteps($form));

        # get num section of step 1
        $steps = $form->getSteps();
        $steps->getIterator()->rewind();
        $oneStep = $steps->getIterator()->current();
        $this->assertEquals(0, $this->getNumSections($oneStep));

        # get num field of step 1 / section 1
        $sections = $oneStep->getSections();
        $sections->getIterator()->rewind();
        $oneSection = $sections->getIterator()->current();
        $this->assertNull($oneSection);
    }

    private function getInputDataSimple_InvalidFieldImplementation() {
        return ['steps'    => ['step_1'],
            'sections' => ['step_1' => ['section_1'] ],
            'fields'   => ['section_1' =>
                [
                    'field1' => ['TextField'        => '_CORE_']
                ]
            ]
        ];
    }

    public function testCreationFormSimple_WithWrongFieldImplementation()
    {
        $this->expectException(\InvalidArgumentException::class);
        $data = $this->getInputDataSimple_InvalidFieldImplementation();
        # get form
        $form = $this->buildForm($data);
    }

    private function getInputDataSimple_InvalidFieldClassName() {
        return ['steps'    => ['step_1'],
            'sections' => ['step_1' => ['section_1'] ],
            'fields'   => ['section_1' =>
                [
                    'field1' => ['TextFieldTTT'        => 'Core']
                ]
            ]
        ];
    }

    public function testCreationFormSimple_WithWrongFieldClassName()
    {
        $this->expectException(\InvalidArgumentException::class);
        $data = $this->getInputDataSimple_InvalidFieldClassName();
        # get form
        $form = $this->buildForm($data);
    }

    private function getInputDataSimple_SameFieldId() {
        return ['steps'    => ['step_1'],
            'sections' => ['step_1' => ['section_1'] ],
            'fields'   => ['section_1' =>
                [
                    'field_1' => ['TextField'        => 'Core'],
                    'field_1' => ['TextField'        => 'Core']
                ]
            ]
        ];
    }

    public function testCreationFormSimple_WithSameFieldId()
    {
        $data = $this->getInputDataSimple_SameFieldId();
        # get form
        $form = $this->buildForm($data);

        # get num step
        $this->assertEquals(1, $this->getNumSteps($form));

        # get num section of step 1
        $steps = $form->getSteps();
        $steps->getIterator()->rewind();
        $oneStep = $steps->getIterator()->current();
        $this->assertEquals(1, $this->getNumSections($oneStep));

        # get num field of step 1 / section 1
        $sections = $oneStep->getSections();
        $sections->getIterator()->rewind();
        $oneSection = $sections->getIterator()->current();
        $this->assertEquals(1, $this->getNumFields($oneSection));
    }


    /**
     * @param $data
     * @return Form
     */
    private function buildForm($data)
    {
        return $this->formWorker->buildForm($data['steps'], $data['sections'], $data['fields']);
    }

    private function getNumSteps(Form $form) : int
    {
        /** @var FormStepCollection $steps */
        $steps = $form->getSteps();
        return $steps->getSize();
    }

    private function getNumSections(FormStep $step) : int
    {
        /** @var FormSectionCollection $sections */
        $sections = $step->getSections();
        return $sections->getSize();
    }

    private function getNumFields(FormSection $section) : int
    {
        /** @var FormSectionCollection $fields */
        $fields = $section->getFields();
        return $fields->getSize();
    }

}