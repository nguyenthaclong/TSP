<?php
namespace Tests\unit;

use Codeception\Test\Unit;
use TSP\Actors\BoundaryDataFactory;
use TSP\Actors\User\UseCases\CreateAccount\Boundaries\NotifierInterface;
use TSP\Actors\User\UseCases\CreateAccount\Boundaries\RepositoryInterface;
use TSP\Actors\User\UseCases\CreateAccount\CreateAccount;
use TSP\Actors\User\UseCases\CreateAccount\RequestData;
use TSP\Actors\User\UseCases\CreateAccount\ResponseData;
use TSP\Entities\Account\Account;
use TSP\Entities\Account\AccountStatus;
use TSP\Entities\EntityFactory;
use TSP\TestApi\DataModelHelper;

class UserCreateAccountTest extends Unit
{
    /**
     * @var CreateAccount
     */
    protected $useCase;

    /**
     * @var RequestData
     */
    protected $requestData;

    /**
     * @var ResponseData
     */
    protected $responseData;

    /**
     * @var  RepositoryInterface
     */
    protected $repository;

    /**
     * @var  NotifierInterface
     */
    protected $notifier;

    /**
     * @var array
     */
    protected $requestDataStruct;

    /**
     * @var DataModelHelper
     */
    protected $dataStructHelper;

    /**
     * @var array
     */
    protected $responseDataStruct;

    public function setUp()
    {
        # set up objects
        $this->repository = $this->getMockForAbstractClass(RepositoryInterface::class);
        $this->notifier   = $this->getMockForAbstractClass(NotifierInterface::class);

        # setup data structure
        $this->dataStructHelper = new DataModelHelper(new BoundaryDataFactory(), 'CreateAccount');
    }


    public function testCreateAccountRegular()
    {
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData();

        # ensure password still passed
        $request->password = ":s':]2hD!I";

        # prepare response data
        /** @var Account $accounteturned */
        $account        = EntityFactory::getInstanceAccount();
        $accountReturned = $account->create($request->toArray());

        # prepare internal components behaviour
        $this->repository->expects($this->exactly(1))
                         ->method('persist')
                         ->will($this->returnValue($accountReturned));

        $this->notifier->expects($this->exactly(1))
                       ->method('notify')
                       ->will($this->returnValue(true));

        # invoke use case
        $this->useCase      = new CreateAccount($this->repository, $this->notifier);
        /** @var ResponseData $response */
        $response = $this->useCase->doCreateAccount($request);

        # assert response has same mandatory properties then request
        $assertable = $request->getMandatoryFields();
        foreach ($assertable as $field) {
            $this->assertEquals($response->$field, $request->$field);
        }

        # assert request has date create
        $this->assertNotEmpty($response->createdDate);

        # assert request has status newly created
        $this->assertEquals($response->status, AccountStatus::NEWLY_CREATED);

    }
}