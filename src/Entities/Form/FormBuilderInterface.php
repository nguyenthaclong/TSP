<?php
namespace TSP\Entities\Form;

use TSP\Entities\Form\Field\AbstractField;

interface FormBuilderInterface {
    public function buildSteps(array $stepsData);
    public function buildSections(array $sectionsData);
    public function buildFields(array $fieldsData);
    public function getForm() : Form;
}