<?php
namespace TSP\Entities\Form;

use TSP\Entities\Form\Field\AbstractField;

class FormSectionIterator implements \Iterator
{
    /** @var int */
    private $positionMap;

    /** @var array */
    private $mapKeys;

    /** @var FormSectionCollection */
    private $sections;

    public function __construct(FormSectionCollection $sections)
    {
        $this->positionMap = 0;
        $this->sections = $sections;
        $this->mapKeys  = array_keys($this->sections->getSections());
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        if ($this->valid()) {
            $key = $this->mapKeys[$this->positionMap];
            return $this->sections->getSections()[$key];
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        ++ $this->positionMap;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return  $this->mapKeys[$this->positionMap];
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset($this->mapKeys[$this->positionMap]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->positionMap = 0;
    }

}