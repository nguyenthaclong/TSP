<?php
namespace TSP\Entities\Form;

use IteratorAggregate;
use JsonSerializable;

class FormSectionCollection implements IteratorAggregate, JsonSerializable
{
    /** @var FormSection[]  */
    private $sections = [];

    /**
     * @param FormSection $section
     */
    public function addSection(FormSection $section)
    {
        if (!array_key_exists($section->getName(), $this->sections)) {
            $this->sections[$section->getName()] = $section;
        }
    }

    /**
     * @param FormSection $section
     */
    public function removeSection(FormSection $section)
    {
        if (array_key_exists($section->getName(), $this->sections)) {
            unset($this->sections[$section->getName()]);
        }
    }

    /**
     * @return FormSection[]
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * @return int
     */
    public function getSize() : int
    {
        return count(array_keys($this->sections));
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new FormSectionIterator($this);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $out = [];
        foreach ($this->sections as $section) {
            $out[] = $section->jsonSerialize();
        }
        return $out;
    }
}