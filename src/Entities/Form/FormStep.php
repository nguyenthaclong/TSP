<?php


namespace TSP\Entities\Form;


class FormStep implements \JsonSerializable
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var FormSectionCollection */
    private $sections;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|FormSectionCollection
     */
    public function getSections(): FormSectionCollection
    {
        return (isset($this->sections) ? $this->sections : new FormSectionCollection());
    }

    /**
     * @param FormSectionCollection $sections
     */
    public function setSections(FormSectionCollection $sections): void
    {
        $this->sections = $sections;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $out[$this->name] = $this->sections->jsonSerialize();
    }
}