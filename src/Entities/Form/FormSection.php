<?php
namespace TSP\Entities\Form;

use TSP\Entities\Form\Field\AbstractField;
use TSP\Entities\Form\Field\FieldCollection;

class FormSection implements \JsonSerializable
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var FieldCollection */
    private $fields;

    /**
     * FormSection constructor
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|FieldCollection
     */
    public function getFields(): FieldCollection
    {
        return $this->fields;
    }

    /**
     * @param FieldCollection $fields
     */
    public function setFields(FieldCollection $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $out[$this->name] = $this->fields->jsonSerialize();
    }
}