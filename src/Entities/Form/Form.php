<?php
namespace TSP\Entities\Form;

class Form implements \JsonSerializable
{
    /** @var string */
    private $name;

    /** @var FormStepCollection */
    private $steps;

    /**
     * @param FormStepCollection $steps
     */
    public function setSteps(FormStepCollection $steps)
    {
        $this->steps = $steps;
    }

    /**
     * @return null|FormStepCollection
     */
    public function getSteps(): FormStepCollection
    {
        return (isset($this->steps) ? $this->steps : new FormStepCollection());
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [$this->name => ['steps' => $this->steps->jsonSerialize() ] ];
    }
}