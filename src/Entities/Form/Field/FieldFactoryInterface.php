<?php
namespace TSP\Entities\Form\Field;

interface FieldFactoryInterface
{
    public static function createField(string $name, string $className, string $typeImplementation): FieldInterface;
}