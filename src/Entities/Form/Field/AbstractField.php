<?php
namespace TSP\Entities\Form\Field;

use TSP\Entities\Form\Field\Boundaries\FieldFeedInterface;
use TSP\Entities\Form\Field\Boundaries\FieldRepositoryInterface;

abstract class AbstractField implements FieldInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $label;

    /** @var FieldFeedInterface */
    protected $feed;

    /** @var FieldRepositoryInterface */
    protected $repository;

    /**
     * AbstractField constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    abstract public function validate(): bool ;

    abstract public function isMandatory(): bool ;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return FieldFeedInterface
     */
    public function getFeed(): FieldFeedInterface
    {
        return $this->feed;
    }

    /**
     * @param FieldFeedInterface $feed
     */
    public function setFeed(FieldFeedInterface $feed): void
    {
        $this->feed = $feed;
    }

    /**
     * @return FieldRepositoryInterface
     */
    public function getRepository(): FieldRepositoryInterface
    {
        return $this->repository;
    }

    /**
     * @param FieldRepositoryInterface $repository
     */
    public function setRepository(FieldRepositoryInterface $repository): void
    {
        $this->repository = $repository;
    }
}