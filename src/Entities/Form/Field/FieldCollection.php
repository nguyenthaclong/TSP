<?php
namespace TSP\Entities\Form\Field;

use IteratorAggregate;
use JsonSerializable;

class FieldCollection implements IteratorAggregate, JsonSerializable
{
    /** @var FieldInterface[]  */
    private $fields = [];

    /**
     * @param FieldInterface $field
     */
    public function addField(FieldInterface $field)
    {
        if (!array_key_exists($field->getName(), $this->fields)) {
            $this->fields[$field->getName()] = $field;
        }
    }

    /**
     * @param FieldInterface $field
     */
    public function removeField(FieldInterface $field)
    {
        if (array_key_exists($field->getName(), $this->fields)) {
            unset($this->fields[$field->getName()]);
        }
    }

    /**
     * @return FieldInterface[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return int
     */
    public function getSize() : int
    {
        return count(array_keys($this->fields));
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new FieldIterator($this);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $out = [];
        foreach ($this->fields as $field) {
            $out[] = $field->jsonSerialize();
        }
        return $out;
    }
}