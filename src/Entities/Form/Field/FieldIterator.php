<?php
namespace TSP\Entities\Form\Field;

class FieldIterator implements \Iterator
{
    /** @var int */
    private $positionMap;

    /** @var array */
    private $mapKeys;

    /** @var FieldCollection */
    private $fields;

    public function __construct(FieldCollection $fields)
    {
        $this->positionMap = 0;
        $this->fields = $fields;
        $this->mapKeys  = array_keys($this->fields->getFields());
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        if ($this->valid()) {
            $key = $this->mapKeys[$this->positionMap];
            return $this->fields->getFields()[$key];
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        ++ $this->positionMap;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return  $this->mapKeys[$this->positionMap];
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset($this->mapKeys[$this->positionMap]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->positionMap = 0;
    }
}