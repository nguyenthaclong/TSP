<?php
namespace TSP\Entities\Form\Field;
use TSP\Entities\Form\Field\AbstractField;

class FieldFactory implements FieldFactoryInterface
{
    /**
     * @param string $name
     * @param string $className
     * @param string $typeImplementation
     * @return FieldInterface
     */
    public static function createField(string $name, string $className, string $typeImplementation): FieldInterface
    {
        $srcDir                  = dirname(dirname(dirname(dirname(__FILE__))));
        $fieldNamespace          = FieldConstants::FIELD_NAMESPACE;
        $fieldAbstractPath       = $srcDir .FieldConstants::ABSTRACT_FIELD_RELATIVE_PATH;

        if ($typeImplementation === FieldConstants::CORE_FIELD_IDENTIFIER)
        {
            $absoluteClassPath = $srcDir .FieldConstants::CORE_FIELD_RELATIVE_PATH."{$className}.php";
            $nameSpaceClass    = FieldConstants::CORE_FIELD_NAMESPACE;

        } elseif ($typeImplementation === FieldConstants::EXTENDED_FIELD_IDENTIFIER) {

            $absoluteClassPath = $srcDir .FieldConstants::EXTENDED_FIELD_RELATIVE_PATH."{$className}.php";
            $nameSpaceClass    = FieldConstants::EXTENDED_FIELD_NAMESPACE;

        } else {
            throw new \InvalidArgumentException("Field identifier not allowed", 403);
        }

        if (false === file_exists($absoluteClassPath)) {
            throw new \InvalidArgumentException("Class file {$absoluteClassPath} not found", 404);
        }
        try {
            require_once $absoluteClassPath;
            require_once $fieldAbstractPath;
            $fullClassName = $nameSpaceClass . $className;
            $class = new \ReflectionClass($fullClassName);
            if ($class->isSubclassOf($fieldNamespace.'AbstractField')) {
                return new $fullClassName($name);
            } else {
                throw new \InvalidArgumentException("Interface expected not found", 403);
            }
        } catch (\ReflectionException $e) {
            throw new \InvalidArgumentException("Reflection class exception: [{$e->getMessage()}]", 500);
        }
    }
}