<?php
namespace TSP\Entities\Form\Field\Boundaries;

use TSP\Entities\Form\Field\AbstractField;

/**
 * Interface FieldRepositoryInterface
 * @package TSP\Entities\Form\Field\Boundaries
 * To store field in some repository
 */
interface FieldRepositoryInterface {

    /**
     * @param AbstractField $field
     * @return AbstractField
     */
    public function store(AbstractField $field) : AbstractField;

}