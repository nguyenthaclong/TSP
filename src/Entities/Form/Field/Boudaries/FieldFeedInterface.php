<?php
namespace TSP\Entities\Form\Field\Boundaries;

use TSP\Entities\Form\Field\AbstractField;

/**
 * Interface FieldInputInterface
 * @package TSP\Entities\Form\Field\Boundaries
 * Implements to initialize field data
 */
interface FieldFeedInterface {

    /**
     * @param AbstractField $field
     * @return AbstractField
     */
    public function feed(AbstractField $field) : AbstractField;

}