<?php
namespace TSP\Entities\Form\Field;

interface FieldInterface
{
    public function jsonSerialize();
}