<?php
namespace TSP\Entities\Form\Field;

trait TraitField {

    public function validate(): bool {
        return true;
    }

    public function isMandatory(): bool {
        return false;
    }
    
}