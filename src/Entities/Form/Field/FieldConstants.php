<?php
namespace TSP\Entities\Form\Field;

/**
 * Class FieldConstantNamespaces
 * @package TSP\Entities\Form\Field
 * Used by field factory to make instance of concretes fields objects
 */
final class FieldConstants
{
    CONST FIELD_NAMESPACE              = "\\TSP\\Entities\\Form\\Field\\";
    CONST ABSTRACT_FIELD_RELATIVE_PATH = "/Entities/Form/Field/AbstractField.php";

    CONST CORE_FIELD_RELATIVE_PATH     = "/Actors/Admin/Form/Fields/";
    CONST CORE_FIELD_NAMESPACE         = "\\TSP\\Actors\\Admin\\Form\\Fields\\";

    CONST EXTENDED_FIELD_RELATIVE_PATH = "/Actors/Admin/Form/Fields/Extensions/";
    CONST EXTENDED_FIELD_NAMESPACE     = "\\TSP\\Actors\\Admin\\Form\\Fields\\Extensions\\";

    CONST CORE_FIELD_IDENTIFIER        = "Core";
    CONST EXTENDED_FIELD_IDENTIFIER    = "Extension";


}