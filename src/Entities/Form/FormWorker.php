<?php
namespace TSP\Entities\Form;

use TSP\Entities\Form\Field\FieldCollection;
use TSP\Entities\Form\Field\FieldFactory;

class FormWorker implements FormBuilderInterface
{
    /** @var Form */
    private $form;

    public function __construct()
    {
        $this->form = new Form();
    }

    /**
     * @param array $stepsData
     * @param array $sectionsData
     * @param array $fieldsData
     * @return Form
     * @throws \InvalidArgumentException
     */
    public function buildForm(array $stepsData, array $sectionsData, array $fieldsData) : Form
    {
        $this->buildSteps($stepsData);
        $this->buildSections($sectionsData);
        $this->buildFields($fieldsData);
        return $this->getForm();
    }

    /**
     * @param array $stepsData
     */
    public function buildSteps(array $stepsData)
    {
        $formSteps = new FormStepCollection();
        foreach ($stepsData as $step) {
            $formStep = new FormStep($step);
            $formSteps->addStep($formStep);
        }
        $this->form->setSteps($formSteps);
    }

    /**
     * @param array $sectionsData
     */
    public function buildSections(array $sectionsData)
    {
        $steps = $this->form->getSteps();
        if ($steps === null) {
            return;
        }
        foreach ($steps as $step) {
            $formSections = new FormSectionCollection();
            if (!array_key_exists($step->getName(), $sectionsData)) {
                continue;
            }
            $sectionNames = $sectionsData[$step->getName()];
            foreach ($sectionNames as $sectionName) {
                $formSection = new FormSection($sectionName);
                $formSections->addSection($formSection);
            }
            $step->setSections($formSections);
        }
    }

    /**
     * @param array $fieldsData
     */
    public function buildFields(array $fieldsData)
    {
        /** @var FormStepCollection $steps */
        $steps = $this->form->getSteps();

        foreach ($steps as $step) {
            /** @var FormSectionCollection $sections */
            $sections = $step->getSections();

            foreach ($sections as $section) {
                if (!array_key_exists($section->getName(), $fieldsData)) {
                    continue;
                }
                $fields = new FieldCollection();
                $fieldClassInfos = $fieldsData[$section->getName()];
                foreach ($fieldClassInfos as $fieldName => $fieldPrototype) {
                    $className = array_keys($fieldPrototype)[0];
                    $typeImplementation = $fieldPrototype[$className];
                    $field = FieldFactory::createField($fieldName, $className, $typeImplementation);
                    $fields->addField($field);
                }
                $section->setFields($fields);
            }
        }
    }

    public function getForm(): Form
    {
        return $this->form;
    }
}