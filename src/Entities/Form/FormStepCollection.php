<?php
namespace TSP\Entities\Form;

use JsonSerializable;
use IteratorAggregate;

class FormStepCollection implements IteratorAggregate, JsonSerializable
{
    /** @var FormStep[]  */
    private $steps = [];

    /**
     * @param FormStep $step
     */
    public function addStep(FormStep $step)
    {
        if (!array_key_exists($step->getName(), $this->steps)) {
            $this->steps[$step->getName()] = $step;
        }
    }

    /**
     * @param FormStep $step
     */
    public function removeStep(FormStep $step)
    {
        if (array_key_exists($step->getName(), $this->steps)) {
            unset($this->steps[$step->getName()]);
        }
    }

    /**
     * @return FormStep[]
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @return int
     */
    public function getSize() : int
    {
        return count(array_keys($this->steps));
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new FormStepIterator($this);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $out = [];
        foreach ($this->steps as $step) {
            $out[] = $step->jsonSerialize();
        }
        return $out;

    }
}