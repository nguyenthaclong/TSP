<?php
namespace TSP\Entities\Form;

class FormStepIterator implements \Iterator
{
    /** @var int */
    private $positionMap;

    /** @var array */
    private $mapKeys;

    /** @var FormStepCollection */
    private $steps;

    public function __construct(FormStepCollection $sections)
    {
        $this->positionMap = 0;
        $this->steps = $sections;
        $this->mapKeys  = array_keys($this->steps->getSteps());
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        if ($this->valid()) {
            $key = $this->mapKeys[$this->positionMap];
            return $this->steps->getSteps()[$key];
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        ++ $this->positionMap;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return  $this->mapKeys[$this->positionMap];
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset($this->mapKeys[$this->positionMap]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->positionMap = 0;
    }
}