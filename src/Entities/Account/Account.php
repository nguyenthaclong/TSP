<?php
namespace TSP\Entities\Account;

use DateTime;
use InvalidArgumentException;
use TSP\Entities\Account\AccountStatus;
use TSP\Entities\Account\AccountValidator;
use TSP\Entities\AccessPolicy\AccessPolicy;
use TSP\Entities\Account\Exceptions\UpdateCheckIdentityException;
use TSP\Entities\EntityFactory;
use TSP\Entities\EntityInterface;

class Account implements EntityInterface
{
    /**
     * @var string
     */
    private $login;
    
    /**
     * @var string
     */
    private $password;
    
    /**
     * @var string
     */
    private $userName;
    
    /**
     * @var string
     */
    private $email;
    
    /**
     * @var DateTime
     */
    private $createdDate;

    /**
     * @var DateTime
     */
    private $modifiedDate;
    
    /**
     * @var DateTime
     */
    private $lastAccessDate;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var DateTime
     */
    private $birthDate;

    /**
     * @var string
     */
    private $birthCity;

    /**
     * @var string
     */
    private $birthCountry;

    /**
     * @var string
     */
    private $postalAddress;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $postalBox;

    /**
     * @var string
     */
    private $postalCity;

    /**
     * @var string
     */
    private $postalCountry;

    /**
     * @var AccessPolicy
     */
    private $accessPolicy;

    /**
     * @var string
     */
    private $status;

    /**
     * @param EntityInterface $account
     * @return bool
     * @throws Exceptions\BirthDateBadFormatException
     * @throws Exceptions\EmailBadFormatException
     * @throws Exceptions\InvalidStatusException
     * @throws Exceptions\LoginBadFormatException
     * @throws Exceptions\MissingDataInPostalAddressException
     * @throws Exceptions\PasswordBadFormatException
     * @throws Exceptions\UserNameBadFormatException
     */
    public function validate(EntityInterface $account) : bool
    {
        var_dump($account);
        $validator = new AccountValidator();
        $accountStatus    = new AccountStatus();
        $access    = new AccessPolicy();
        /** @var Account $account */
        return $validator->validateInputData($account) &&
               $accountStatus->checkStatus($account->status) &&
               $access->checkAccessPolicy($account);
    }

    /**
     * @param EntityInterface $account1
     * @param EntityInterface $account2
     * @return bool
     */
    public function compareIdentity(EntityInterface $account1, EntityInterface $account2) : bool
    {
        return $account1->login     === $account2->login &&
               $account1->userName  === $account2->userName &&
               $account1->firstName === $account2->firstName &&
               $account1->lastName  === $account2->lastName &&
               $account1->birthDate === $account2->birthDate &&
               $account1->birthCity === $account2->birthCity;
    }

    /**
     * @param array $data
     * @return EntityInterface
     * @throws InvalidArgumentException
     */
    public function mergeArrayInObject(array $data) : EntityInterface
    {
        $account = new Account();
        foreach($data as $propName => $propValue) {
            if (!property_exists($account, $propName)) {
                throw new InvalidArgumentException();
            }
            switch ($propName) {
                case 'birthDate' :
                case 'createdDate' :
                case 'modifiedDate' :
                    try {
                        $account->$propName = new DateTime($propValue);
                    } catch (\Exception $e) {
                        throw new InvalidArgumentException();
                    }
                    break;
                default :
                    $account->$propName = $propValue;
            }

        }
        return $account;
    }

    /**
     * @param array $data
     * @return EntityInterface
     * @throws Exceptions\BirthDateBadFormatException
     * @throws Exceptions\EmailBadFormatException
     * @throws Exceptions\InvalidStatusException
     * @throws Exceptions\LoginBadFormatException
     * @throws Exceptions\MissingDataInPostalAddressException
     * @throws Exceptions\PasswordBadFormatException
     * @throws Exceptions\UserNameBadFormatException
     * @throws InvalidArgumentException
     */
    public function create(array $data) : EntityInterface
    {
        /** @var Account $account */
        $account = $this->mergeArrayInObject($data);

        # set default status
        $account->status = AccountStatus::NEWLY_CREATED;

        # grant default access
        $access = new AccessPolicy();
        $access->grantDefaultAccess($account);

        # set created date
        $account->createdDate = new DateTime('now');

        # validate
        $this->validate($account);

        return $account;
    }

    /**
     * @param EntityInterface $account
     * @param array $data
     * @return EntityInterface
     * @throws Exceptions\BirthDateBadFormatException
     * @throws Exceptions\EmailBadFormatException
     * @throws Exceptions\InvalidStatusException
     * @throws Exceptions\LoginBadFormatException
     * @throws Exceptions\MissingDataInPostalAddressException
     * @throws Exceptions\PasswordBadFormatException
     * @throws Exceptions\UserNameBadFormatException
     * @throws UpdateCheckIdentityException
     * @throws InvalidArgumentException
     */
    public function update(EntityInterface $account, array $data) : EntityInterface
    {
        /** @var Account $accountUpdated */
        $accountUpdated = $this->mergeArrayInObject($data);

        # check if its the same account
        /** @var Account $account */
        if (false === $this->compareIdentity($account, $accountUpdated )) {
            throw new UpdateCheckIdentityException();
        }

        # set modified date
        $account->modifiedDate = new DateTime('now');

        # validate
        $this->validate($accountUpdated);

        return $accountUpdated;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @return DateTime
     */
    public function getModifiedDate(): DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @return DateTime
     */
    public function getLastAccessDate(): DateTime
    {
        return $this->lastAccessDate;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return DateTime
     */
    public function getBirthDate(): DateTime
    {
        return $this->birthDate;
    }

    /**
     * @return string
     */
    public function getBirthCity(): string
    {
        return $this->birthCity;
    }

    /**
     * @return string
     */
    public function getBirthCountry(): string
    {
        return $this->birthCountry;
    }

    /**
     * @return string
     */
    public function getPostalAddress(): string
    {
        return $this->postalAddress;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getPostalBox(): string
    {
        return $this->postalBox;
    }

    /**
     * @return string
     */
    public function getPostalCity(): string
    {
        return $this->postalCity;
    }

    /**
     * @return string
     */
    public function getPostalCountry(): string
    {
        return $this->postalCountry;
    }

    /**
     * @return AccessPolicy
     */
    public function getAccessPolicy(): AccessPolicy
    {
        return $this->accessPolicy;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

}