<?php
namespace TSP\Entities\Account;

use TSP\Entities\Account\Exceptions\BirthDateBadFormatException;
use TSP\Entities\Account\Exceptions\EmailBadFormatException;
use TSP\Entities\Account\Exceptions\LoginBadFormatException;
use TSP\Entities\Account\Exceptions\MissingDataInPostalAddressException;
use TSP\Entities\Account\Exceptions\PasswordBadFormatException;
use TSP\Entities\Account\Exceptions\UserNameBadFormatException;

class AccountValidator
{
    /**
     * @param Account $account
     * @return bool
     * @throws BirthDateBadFormatException
     * @throws EmailBadFormatException
     * @throws LoginBadFormatException
     * @throws MissingDataInPostalAddressException
     * @throws PasswordBadFormatException
     * @throws UserNameBadFormatException
     */
    public function validateInputData(Account $account) : bool
    {
        return $this->validateLogin($account->getLogin()) &&
               $this->validatePassword($account->getPassword()) &&
               $this->validateEmail($account->getEmail()) &&
               $this->validateUserName($account->getUserName()) &&
               $this->validateBirthDate($account->getBirthDate()) &&
               $this->validatePostalAddress($account);
    }

    /**
     * @param string $login
     * @return bool
     * @throws LoginBadFormatException
     */
    public function validateLogin(string $login) : bool
    {
        # not allowed length
        if (strlen($login) > 32 || strlen($login) < 5) {
            throw new LoginBadFormatException();
        }

        # not allowed characters
        $notAllowed = '/[^a-zA-Z0-9_.]+/';
        if (1 === preg_match($notAllowed, $login)) {
            throw new LoginBadFormatException();
        }
        echo 'validateLogin ... OK';
        return true;
    }

    /**
     * @param string $password
     * @return bool
     * @throws PasswordBadFormatException
     */
    public function validatePassword(string $password) : bool
    {
        # not allowed length
        if (strlen($password) > 16 || strlen($password) < 5) {
            throw new PasswordBadFormatException();
        }

        #
        $mustContains = '/([a-z]+)|([A-Z]+)|([0-9]+)|([^a-zA-Z0-9_]+)/';
        $matches = [];
        $numMatched = preg_match_all($mustContains, $password, $matches,PREG_PATTERN_ORDER, 0);
        if ($numMatched !== false) {
            for($i=1;$i<count($matches);$i++) {
                $matchFiltered = array_filter($matches[$i], function($item) {return !empty($item);});
                if (count($matchFiltered) === 0) {
                    throw new PasswordBadFormatException();
                }
            }
            echo 'validatePassword ... OK';
            return true;
        }

        throw new PasswordBadFormatException();
    }

    /**
     * @param string $email
     * @return bool
     * @throws EmailBadFormatException
     */
    public function validateEmail(string $email) : bool
    {
        echo 'validateEmail ... ';
        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new EmailBadFormatException();
        }
        return true;
    }

    /**
     * @param string $userName
     * @return bool
     * @throws UserNameBadFormatException
     */
    public function validateUserName(string $userName) : bool
    {
        # not allowed length
        if (strlen($userName) > 32 || strlen($userName) < 5) {
            throw new UserNameBadFormatException();
        }

        # not allowed characters
        $notAllowed = '/[^a-zA-Z0-9_.@\s]+/';
        if (1 === preg_match($notAllowed, $userName)) {
            throw new UserNameBadFormatException();
        }
        echo 'validateUserName ... OK';
        return true;
    }

    /**
     * @param string|\DateTime $birthDate
     * @return bool
     * @throws BirthDateBadFormatException
     */
    public function validateBirthDate($birthDate)
    {
        if (!$birthDate instanceof \DateTime) {
            $pregDate = '/^[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])/';
            if (!empty($birthDate) && (1 !== preg_match($pregDate, $birthDate))) {
                    throw new BirthDateBadFormatException();
            }
            try {
                $birthDate = new \DateTime($birthDate. ' 00:00');
            } catch (\Exception $e) {
                throw new BirthDateBadFormatException();
            }
        }

        # check birth date < today
        $now = new \DateTime('now');

        echo 'validateBirthDate ... ';
        return ($birthDate < $now);
    }

    /**
     * @param Account $account
     * @return bool
     * @throws MissingDataInPostalAddressException
     */
    public function validatePostalAddress(Account $account)
    {
        echo 'validatePostalAddress ... ';
        if  (empty($account->getPostalAddress()) ||
             empty($account->getPostalCode()) ||
             empty($account->getPostalCity())) {
            throw new MissingDataInPostalAddressException();
        }
        return true;
    }
}