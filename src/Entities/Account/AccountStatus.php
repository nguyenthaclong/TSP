<?php
namespace TSP\Entities\Account;


use TSP\Entities\Account\Exceptions\InvalidStatusException;

final class AccountStatus
{
    const NEWLY_CREATED   = 'Newly created';
    const VALID           = 'Valid';
    const INVALID         = 'Invalid';
    const DELETED         = 'Deleted';
    const BLOCKED         = 'Blocked';
    const TO_BE_ACTIVATED = 'To be activated';

    /**
     * @param $status string
     * @return bool
     * @throws InvalidStatusException
     */
    public function checkStatus(string $status) : bool
    {
        if ($status !== self::NEWLY_CREATED && $status !== self::INVALID && $status !== self::DELETED &&
            $status !== self::VALID && $status !== self::BLOCKED && $status !== self::TO_BE_ACTIVATED) {
            throw new InvalidStatusException();
        }
        return true;
    }

}