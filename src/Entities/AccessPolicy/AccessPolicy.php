<?php
namespace TSP\Entities\AccessPolicy;


use TSP\Entities\Account\Account;

class AccessPolicy
{
    /**
     * @param Account $account
     */
    public function grantDefaultAccess(Account $account)
    {
        
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function checkAccessPolicy(Account $account)
    {
        return true;
    }
}