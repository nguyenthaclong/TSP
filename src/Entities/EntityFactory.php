<?php
namespace TSP\Entities;

use TSP\Entities\Account\Account;
use TSP\Entities\Form\Form;

class EntityFactory
{
    public static function getInstanceAccount() : Account
    {
        return new Account();
    }

    public static function getInstanceForm() : Form
    {
        return new Form();
    }
}