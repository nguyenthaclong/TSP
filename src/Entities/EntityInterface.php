<?php

namespace TSP\Entities;

interface EntityInterface {

    /**
     * @param EntityInterface $entity1
     * @param EntityInterface $entity2
     * @return bool
     */
    public function compareIdentity(EntityInterface $entity1, EntityInterface $entity2) : bool;

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function validate(EntityInterface $entity) : bool;

    /**
     * @param array $data
     * @return EntityInterface
     * @throws \InvalidArgumentException
     */
    public function mergeArrayInObject(array $data) : EntityInterface;

    /**
     * @param array $data
     * @return EntityInterface
     * @throws \InvalidArgumentException
     */
    public function create(array $data) : EntityInterface;

    /**
     * Update data
     * @param EntityInterface $entity
     * @param array $data
     * @return EntityInterface
     * @throws \InvalidArgumentException
     */
    public function update(EntityInterface $entity, array $data) : EntityInterface;

}