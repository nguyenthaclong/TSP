<?php
namespace TSP\Actors;

use ReflectionClass;
use ReflectionProperty;

abstract class AbstractDataStructure implements BoundaryDataInterface
{
    abstract public function getDateFields() : array;

    abstract public function getMandatoryFields() : array;

    abstract public function isEmpty() : bool;

    public function toArray(): array
    {
        try {
            $reflect = new ReflectionClass($this);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
            $output = [];
            foreach ($props as $prop) {
                $output[$prop->getName()] = $prop->getValue($this);
            }
            return $output;
        } catch (\Exception $e) {
            return [];
        }
    }
}