<?php
namespace TSP\Actors\User\UseCases\CreateAccount;

use TSP\Actors\AbstractDataStructure;

final class ResponseData extends AbstractDataStructure {

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $userName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $createdDate;

    /**
     * @var string
     */
    public $gender;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $birthDate;

    /**
     * @var string
     */
    public $birthCity;

    /**
     * @var string
     */
    public $birthCountry;

    /**
     * @var string
     */
    public $postalAddress;

    /**
     * @var string
     */
    public $postalCode;

    /**
     * @var string
     */
    public $postalBox;

    /**
     * @var string
     */
    public $postalCity;

    /**
     * @var string
     */
    public $postalCountry;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $error;


    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->login) && empty($this->userName) &&
                empty($this->email)  && empty($this->createdDate) && empty($this->status));
    }

    public function getMandatoryFields(): array
    {
        return ['login', 'userName', 'email', 'createdDate', 'status'];
    }

    public function getDateFields(): array
    {
        return ['birthDate'   => 'Y-m-d',
                'createdDate' => 'Y-m-d H:i'];
    }

}