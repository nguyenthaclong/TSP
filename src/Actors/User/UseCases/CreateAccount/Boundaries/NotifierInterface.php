<?php
namespace TSP\Actors\User\UseCases\CreateAccount\Boundaries;

interface NotifierInterface {

    public function notify(string $address) : bool ;

}