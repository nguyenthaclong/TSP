<?php
namespace TSP\Actors\User\UseCases\CreateAccount\Boundaries;

use RuntimeException;
use TSP\Entities\Account\Account;

/**
 * Interface EntityRepositoryInterface
 * Assure inverse dependency abstract link with REPOSITORIES COMPONENT
 */

interface RepositoryInterface {

    /**
     * @param Account $account
     * @return Account
     * @throws RuntimeException
     */
    public function persist(Account $account) : Account;


}