<?php
namespace TSP\Actors\User\UseCases\CreateAccount;

use ReflectionClass;
use TSP\Actors\AbstractDataStructure;

final class RequestData extends AbstractDataStructure {

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $userName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $gender;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $birthDate;

    /**
     * @var string
     */
    public $birthCity;

    /**
     * @var string
     */
    public $birthCountry;

    /**
     * @var string
     */
    public $postalAddress;

    /**
     * @var string
     */
    public $postalCode;

    /**
     * @var string
     */
    public $postalBox;

    /**
     * @var string
     */
    public $postalCity;

    /**
     * @var string
     */
    public $postalCountry;

     /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->login) && empty($this->userName) && empty($this->email));
    }

    public function getMandatoryFields(): array
    {
        return ['login', 'userName', 'email'];
    }

    public function getDateFields(): array
    {
        return ['birthDate' => 'Y-m-d'];
    }

}