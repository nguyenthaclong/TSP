<?php
namespace TSP\Actors\User\UseCases\CreateAccount;

use TSP\Actors\BoundaryDataInterface;
use TSP\Actors\User\UseCases\CreateAccount\Boundaries\NotifierInterface;
use TSP\Actors\User\UseCases\CreateAccount\Boundaries\RepositoryInterface;
use TSP\Actors\User\UseCases\CreateAccount\Boundaries\RequesterInterface;
use TSP\Entities\Account\Account;
use TSP\Entities\EntityFactory;

class CreateAccount implements RequesterInterface
{

    /** @var RepositoryInterface */
    private $repository;

    /** @var NotifierInterface */
    private $notifier;

    /**
     * @param RepositoryInterface $repository
     * @param NotifierInterface $notifier
     */
    public function __construct(RepositoryInterface $repository,
                                NotifierInterface $notifier)
    {
        $this->repository = $repository;
        $this->notifier    = $notifier;
    }

    /**
     * @param BoundaryDataInterface $requestData
     * @return BoundaryDataInterface
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Exception
     */
    public function doCreateAccount(BoundaryDataInterface $requestData): BoundaryDataInterface
    {
        # get input data
        /** @var RequestData $input */
        $input = $requestData;

        # use case scenario here
        /** @var Account $account */
        $account = EntityFactory::getInstanceAccount();
        $accountCreated = $account->create($input->toArray());

        # if creation is done, it means all data is validated, we persist account
        /** @var Account $accountCreated */
        $accountPersisted = $this->repository->persist($accountCreated);

        # prepare response data
        $output = new ResponseData();
        $output->login       = $accountPersisted->getLogin();
        $output->userName    = $accountPersisted->getUserName();
        $output->email       = $accountPersisted->getEmail();
        $output->status      = $accountPersisted->getStatus();
        $output->createdDate = $accountPersisted->getCreatedDate();

        # notify
        $this->notifier->notify($output->email);

        return $output;
    }
}