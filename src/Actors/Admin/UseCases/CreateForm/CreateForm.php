<?php
namespace TSP\Actors\Admin\UseCases\CreateForm;

use TSP\Actors\Admin\UseCases\CreateForm\Boundaries\RepositoryInterface;
use TSP\Actors\Admin\UseCases\CreateForm\Boundaries\RequesterInterface;
use TSP\Actors\BoundaryDataInterface;
use TSP\Entities\Form\Form;
use TSP\Entities\Form\FormWorker;

class CreateForm implements RequesterInterface
{
    /** @var RepositoryInterface */
    private $repository;

    /**
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function doCreateForm(BoundaryDataInterface $requestData): BoundaryDataInterface
    {
        # get input data
        /** @var RequestData $input */
        $input = $requestData;

        # use case scenario here
        /** @var Form $form */
        $formWorker = new FormWorker();
        $form = $formWorker->buildForm($input->formSteps, $input->formSections, $input->formFields);
        $form->setName($input->formName);

        # if creation is done, it means all data is validated, we persist form
        /** @var Form $formPersisted */
        $formPersisted = $this->repository->persist($form);

        # prepare response data
        $output = new ResponseData();
        $output->formName = $formPersisted->getName();
        $output->formJson = json_encode($formPersisted);
        return $output;
    }
}