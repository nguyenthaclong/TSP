<?php
namespace TSP\Actors\Admin\UseCases\CreateForm\Boundaries;

use TSP\Actors\BoundaryDataInterface;

/**
 * Interface RequesterInterface
 * Assure inverse dependency abstract link with CONTROLLERS/ADAPTERS LAYER
 * and inverse dependency abstract link USE CASE LAYER
 *
 * Particularity of this separation is :
 * -------------------------------------
 * It assure protection USE CASE layer from CONTROLLER layer changes
 * and also protect CONTROLLER layer from USE CASE changes
 */
interface RequesterInterface {
    public function doCreateForm(BoundaryDataInterface $requestData) : BoundaryDataInterface;
}
