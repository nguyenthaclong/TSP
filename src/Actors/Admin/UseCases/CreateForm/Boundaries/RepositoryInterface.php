<?php
namespace TSP\Actors\Admin\UseCases\CreateForm\Boundaries;

use TSP\Entities\Form\Form;

/**
 * Interface EntityRepositoryInterface
 * Assure inverse dependency abstract link with REPOSITORIES COMPONENT
 */

interface RepositoryInterface {

    /**
     * @param Form $account
     * @return Form
     */
    public function persist(Form $account) : Form;


}