<?php
namespace TSP\Actors\Admin\UseCases\CreateForm;

use TSP\Actors\AbstractDataStructure;

final class ResponseData extends AbstractDataStructure {

    /**
     * @var string
     */
    public $formName;

    /**
     * @var array
     */
    public $formJson;

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->formName) && empty($this->formJson));
    }

    public function getMandatoryFields(): array
    {
        return ['formName', 'formJson'];
    }

    public function getDateFields(): array
    {
        return [];
    }
}