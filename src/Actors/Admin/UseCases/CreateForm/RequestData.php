<?php
namespace TSP\Actors\Admin\UseCases\CreateForm;

use TSP\Actors\AbstractDataStructure;

final class RequestData extends AbstractDataStructure {

    /**
     * @var string
     */
    public $formName;

    /**
     * @var array
     */
    public $formSteps;

    /**
     * @var array
     */
    public $formSections;

    /**
     * @var array
     */
    public $formFields;


     /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->formName));
    }

    public function getMandatoryFields(): array
    {
        return ['formName'];
    }

    public function getDateFields(): array
    {
        return [];
    }

}