<?php
namespace TSP\Actors\Admin\Form\Fields;

use TSP\Entities\Form\Field\AbstractField;
use TSP\Entities\Form\Field\TraitField;

class TextField extends AbstractField
{
    use TraitField;

    /** @var int */
    private $maxLength;

    /** @var string */
    private $defaultValue;


    /**
     * @return int
     */
    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    /**
     * @param int $maxLength
     */
    public function setMaxLength(int $maxLength): void
    {
        $this->maxLength = $maxLength;
    }

    /**
     * @return string
     */
    public function getDefaultValue(): string
    {
        return $this->defaultValue;
    }

    /**
     * @param string $defaultValue
     */
    public function setDefaultValue(string $defaultValue): void
    {
        $this->defaultValue = $defaultValue;
    }

    public function jsonSerialize()
    {
        return [$this->getName() => [ 'className' => __CLASS__,
                                      'properties' => ['maxLength' => $this->maxLength,
                                                       'default'   => $this->defaultValue]
                                                      ]
                                    ];
    }

}