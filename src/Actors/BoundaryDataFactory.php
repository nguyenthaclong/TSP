<?php
namespace TSP\Actors;

use TSP\Actors\User\UseCases\CreateAccount\RequestData as CreateAccountRequestData;
use TSP\Actors\User\UseCases\CreateAccount\ResponseData as CreateAccountResponseData;

use TSP\Actors\Admin\UseCases\CreateForm\RequestData as CreateFormRequestData;
use TSP\Actors\Admin\UseCases\CreateForm\ResponseData as CreateFormResponseData;

final class BoundaryDataFactory
{
    /**
     * @param string $useCase
     * @return BoundaryDataInterface
     */
    public function getInstanceRequestData(string $useCase)
    {
        switch ($useCase) {
            case 'CreateAccount' :
                return new CreateAccountRequestData();
            case 'CreateForm' :
                return new CreateFormRequestData();
            default:
                return null;
        }
    }

    /**
     * @param string $useCase
     * @return BoundaryDataInterface
     */
    public function getInstanceResponseData(string $useCase)
    {
        switch ($useCase) {
            case 'CreateAccount' :
                return new CreateAccountResponseData();
            case 'CreateForm' :
                return new CreateFormResponseData();
            default:
                return null;
        }
    }
}